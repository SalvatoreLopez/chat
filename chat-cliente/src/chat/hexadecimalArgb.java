
package chat;

import java.awt.Color;
import javax.swing.JButton;

public class hexadecimalArgb {
    
    //colores
    public String blanco ="FFFFFF";
    public String grisClaro = "757575";
    public String grisOscuro= "f6f7fb";
    public String grisTitulo = "acb0bb";
    public String azul = "2ab1e0";
    public String error = "D32F2F";
    public String fondos = "eeeeee";
    
    //convierte el codiogo de color hexadecimal a RGB 
    public int hexadecimalARGB(String colorHexadecimal){
        return Integer.parseInt(colorHexadecimal, 16 );
    }
    //crear botones
    public JButton crearBotones( String texto,String color,int x, int y,int width ,int  heigh,String colorFuete){
      JButton boton = new JButton();
      boton.setText(texto);
      boton.setBorderPainted(false);
      boton.setBackground(new Color(hexadecimalARGB(color)));
      boton.setBounds(x,y,width,heigh);
      boton.setForeground(new Color(hexadecimalARGB(colorFuete)));
     return  boton;
             
    }
 
    
    

}
