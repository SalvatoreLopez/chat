
package chat;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.net.InetAddress;
import javax.swing.*;

public class login  extends JFrame{
    //mandamos a llamar la clase hexadecimalArgb
    public static String idNick = "";
    hexadecimalArgb hrgb = new hexadecimalArgb();
    
    JLabel tituloLogin1;
    JLabel tituloLogin2;
    JTextField usuario;
    JTextField ip;
    JPasswordField password;
    JButton botonEntrar;
    JButton botonRegistrar;
    JButton botonCerrarLogin;
    JButton bottonCerrar;
    
    public  login() {
        super();
        propiedadesVentanaLogin();
        componetes();
        this.setUndecorated(true);
    }
    
    private void propiedadesVentanaLogin(){
        this.setTitle("Bienvenido a el login del  chat en java");                  
        this.setSize(630, 450);                                 
        this.setLocationRelativeTo(null);                       
        this.setLayout(null);
        this.getContentPane().setBackground(new Color(hrgb.hexadecimalARGB(hrgb.grisOscuro)));
        this.setResizable(false);                               
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
       
        
    }
    
    //componentes
    private void componetes(){
        tituloLogin1 = new JLabel("Bienvenido");
        tituloLogin2 = new JLabel("de Nuevo!");
        usuario = new JTextField();
        password = new JPasswordField();
        ip = new JTextField();
        botonEntrar = new JButton();
        botonRegistrar = new JButton();
        botonCerrarLogin = new JButton();
        bottonCerrar = new JButton();
        tituloLogin1.setVisible(true);
        tituloLogin1.setForeground(new Color (hrgb.hexadecimalARGB(hrgb.grisTitulo)));
        tituloLogin1.setFont(new Font("Arial",Font.BOLD,25));
        tituloLogin1.setBounds(160, 100, 200, 40);
        tituloLogin2.setVisible(true);
        tituloLogin2.setForeground(new Color (hrgb.hexadecimalARGB(hrgb.azul)));
        tituloLogin2.setFont(new Font("Arial",Font.BOLD,25));
        tituloLogin2.setBounds(300, 100, 150, 40);
        tituloLogin1.setForeground(new Color(hrgb.hexadecimalARGB(hrgb.grisClaro)));
        TextPrompt placeholderUsuario = new TextPrompt("Usuario", usuario);
        TextPrompt placeholderPassword = new TextPrompt("Password", password);
        TextPrompt placeholderIp= new TextPrompt("Ingresa la IP del servidor", ip);
        usuario.setBounds(100, 170, 400, 40);
        usuario.setBorder(BorderFactory.createMatteBorder(0, 0, 2, 0, new Color(hrgb.hexadecimalARGB(hrgb.grisTitulo))));
        usuario.setBackground(new Color(hrgb.hexadecimalARGB(hrgb.grisOscuro)));
        usuario.setFont(new Font("Arial",Font.BOLD,14));
        password.setBounds(100, 220, 400, 40);
        password.setBorder(BorderFactory.createMatteBorder(0, 0, 2, 0, new Color(hrgb.hexadecimalARGB(hrgb.grisTitulo))));
        password.setBackground(new Color(hrgb.hexadecimalARGB(hrgb.grisOscuro)));
        password.setFont(new Font("Arial",Font.BOLD,14));
        ip.setBounds(100, 270, 400, 40);
        ip.setBorder(BorderFactory.createMatteBorder(0, 0, 2, 0, new Color(hrgb.hexadecimalARGB(hrgb.grisTitulo))));
        ip.setBackground(new Color(hrgb.hexadecimalARGB(hrgb.grisOscuro)));
        ip.setFont(new Font("Arial",Font.BOLD,14));
        botonEntrar.setText("Entrar");
        botonEntrar.setBounds(170, 350, 120, 40);
        botonEntrar.setBackground(new Color(hrgb.hexadecimalARGB(hrgb.azul)));
        botonEntrar.setForeground(new Color(hrgb.hexadecimalARGB(hrgb.blanco)));
        botonEntrar.setBorderPainted(false);
        botonRegistrar.setText("Crear Cuenta");
        botonRegistrar.setBounds(300, 350, 120, 40);
        botonRegistrar.setBackground(new Color(hrgb.hexadecimalARGB(hrgb.azul)));
        botonRegistrar.setForeground(new Color(hrgb.hexadecimalARGB(hrgb.blanco)));
        botonRegistrar.setBorderPainted(false);
        bottonCerrar.setBounds(0, 120, 80, 80);
        this.add(tituloLogin1);
        this.add(tituloLogin2);
        this.add(usuario);
        this.add(password);
        this.add(botonEntrar);
        this.add(botonRegistrar);
        this.add(ip);
        //this.add(bottonCerrar);
        
        botonEntrar.addActionListener((ActionEvent e) -> {
            chatDP login = new chatDP();
            try{
            boolean rs = login.LoginDP(usuario.getText(),password.getText());
                if(rs==true){
                    idNick = login.obtenerNick(usuario.getText(), password.getText());
                    super.dispose();
                    String sServerIp = ip.getText();  
                    ventanaChat vChat = new ventanaChat();
                    vChat.setVisible(true);
                    //Se crea un hilo con las funciones de escritura hacia el servidor a conectar 
                    ClientThreadEntrada cte = new ClientThreadEntrada(sServerIp,vChat); 
                    //Se crea un hilo con las funciones para leer el flujo de datos entrante del servidor
                    ClientThreadSalida ctl = new ClientThreadSalida(sServerIp,vChat);
                    //se inician los hilos 
                    ctl.start(); 
                    cte.start();
                }else{
                    JOptionPane.showMessageDialog(null,"Usuario incorrecto!!!!!!");
                }
            }catch(Exception ex){
                System.out.println("Error en el Login: "+ex);
            }
        });
        
        botonRegistrar.addActionListener((ActionEvent e) -> {
            super.dispose();
            ventanaRegistrar vr = new ventanaRegistrar();
            vr.setVisible(true);
        });
        
        bottonCerrar.addActionListener((ActionEvent e) -> {
             super.dispose();
        });
}
    

}
