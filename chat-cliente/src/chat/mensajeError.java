
package chat;

import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;

public class mensajeError extends JFrame{
     //mandamos a llamar la clase hexadecimalArgb
    hexadecimalArgb hrgb = new hexadecimalArgb();
    
    JButton botonAceptar;
    JLabel titulomensaje;
    
    
    public  mensajeError() {
        super();
        propiedadesMensajeError();
        componetes();
        this.setUndecorated(true);
    }
    
    public void propiedadesMensajeError(){
               
        this.setSize(300, 200);                                 
        this.setLocationRelativeTo(null);                       
        this.setLayout(null);
        this.getContentPane().setBackground(new Color(hrgb.hexadecimalARGB(hrgb.grisOscuro)));
        this.setResizable(false);                               
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    }
    
    public void componetes(){
        botonAceptar = new JButton();
        titulomensaje = new JLabel("Error Intenta de nuevo");
        titulomensaje.setVisible(true);
        titulomensaje.setForeground(new Color (hrgb.hexadecimalARGB(hrgb.error)));
        titulomensaje.setFont(new Font("Arial",Font.BOLD,20));
        titulomensaje.setBounds(50, 50, 250, 40);
        botonAceptar.setText("Aceptar");
        botonAceptar.setBounds(100, 130, 100, 40);
        botonAceptar.setBackground(new Color(hrgb.hexadecimalARGB(hrgb.error)));
        botonAceptar.setForeground(new Color(hrgb.hexadecimalARGB(hrgb.blanco)));
        botonAceptar.setBorderPainted(false);
        this.add(botonAceptar);
        this.add(titulomensaje);
        
        botonAceptar.addActionListener((ActionEvent e) -> {
            super.dispose();
            ventanaRegistrar vr = new ventanaRegistrar();
            vr.setVisible(true);
        });
        
    }
}
