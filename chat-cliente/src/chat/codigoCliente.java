package chat;
//Librerías necesarias para la conexión básica  
import java.io.BufferedReader;  
import java.io.IOException;  
import java.io.InputStreamReader;  
import java.io.PrintWriter; 
import java.net.InetAddress;  
import java.net.Socket;

//Esta es la clase para la conexión del lado Cliente
public class codigoCliente {

    /*ublic static void main(String[] args) 
    {
        codigoCliente m = new codigoCliente();
    }*/
    
    /*public codigoCliente ()
    {
        //Se define una conexión IP, en este caso local 
        String sServerIp = "localhost";          
        //Se crea un hilo con las funciones de escritura hacia el servidor a conectar 
        ClientThreadEntrada cte = new ClientThreadEntrada(sServerIp); 
        //Se crea un hilo con las funciones para leer el flujo de datos entrante del servidor
        ClientThreadSalida ctl = new ClientThreadSalida(sServerIp);
        //se inician los hilos 
        ctl.start(); 
        cte.start();
    }*/
}

 //clase del cliente salida
/*class ClientThreadSalida extends Thread { 
    //Se define un puerto de conexión para la lectura de datos
    static final int PORT_NUM_IN = 30123; 
    String ip = "";
    String E = "";
    //Se manejan tambien variables para manejar diferentes fases del hilo 
    String cache = null;
    Socket calceta = null;

    public ClientThreadSalida() { 
    }   
    
    public ClientThreadSalida(String ipAdress)
    { 
       //al construir el hilo, se define la IP a conectar 
       ip = ipAdress;     
    }
    
    public void run(){
        try
        {
            //En caso de que la IP no sea proporcionada, se define que sea por default el localhost como la IP a conectar
            InetAddress address = InetAddress.getLocalHost(); 
            Socket s1 = null; 
            String line = null; 
            //se definen los flujpos de lectura 
            BufferedReader br = null; 
            PrintWriter os = null;
            //Se define la IP y se crea el socket en el puerto definido
            try
            {
                if (ip.isEmpty()) 
                { 
                    s1 = new Socket(address, PORT_NUM_IN);
                }                 
                else 
                { 
                    s1 = new Socket(ip, PORT_NUM_IN); 
                }
                //Se definen los flujos de etrada y salida
                br = new BufferedReader(new InputStreamReader(System.in)); 
                os = new PrintWriter(s1.getOutputStream());
            } catch (IOException e){
               //en caso de fallar, se cierran las conexiones y lecturas de flujos
               e.printStackTrace(); 
               System.err.print("Error al inicializar proceso");
               try
               {
                   if (os != null)
                   {
                      os.close(); 
                   }
                   if(br != null)
                   {
                       br.close();
                   }
                   if(s1 != null)
                   {
                       s1.close();
                   }
                   System.out.println("Se ha cerrado la conexión");
               } catch (Exception ecp){
                 System.out.println(">! Ocurrió un error al intentar cerrar el Socket");
                 System.out.println(ecp.getMessage());
                 
                 System.out.println("-------------------------------------------------");
               }
               return;
            }
            //Muestra la conexión local
            System.out.println("Dirección : " + address);
            String response = null;
            
            try
            {
               
            
                //Se le indica al usuario escribir su nick
                System.out.println("Escribe tu Nick");
                line = br.readLine();
                //Se envía a servidor la palabra SYS_SET_NICK junto con el nick para indicarle al servidor
                //que dicho hilo será de tal nombre, definiendo un nick para dicha conexión
                os.println("SYS_SET_NICK" + line);
                //Se limpia el flujo
                os.flush();
                //Se le indica al usuario que ya puede comenzar a chatear
                System.out.println("Ya puedes escribir / Escribe 'EXIT' para cerrar el programa"); 
                line = br.readLine();
                //se define un ciclo semi infinito para mandar lineas de texto al servidor
                
                while (line.compareTo("EXIT") != 0) 
                {                      
                    os.println(line);
                    os.flush();
                    line = br.readLine();
                }
                //En caso de existir algún tipo de error se cierran las conexiones
            } catch (IOException e){
                e.printStackTrace(); 
                System.out.println("SocketInfo: Error de Lectura");
            } finally{
                try
                {
                    if (os != null)
                    {
                        os.close();
                    }
                    if (br != null)
                    {
                        br.close();
                    }
                    if (s1!= null)
                    {
                        s1.close();
                    }
                    System.out.println("Se ha cerrado la conexión");
                } catch (Exception ecp2){
                    System.out.println(">! Ocurrió un error al intentar cerrar el Socket");
                    System.out.println(ecp2.getMessage());
                    System.out.println("-------------------------------------------------");
                }
            }
        } catch (IOException e){
            System.out.println(">#! Error en una conexión");
        }
    }    
    
 }*/

//clase del cliente entrada
/*class ClientThreadEntrada extends Thread {
    //Se definen las variables al igual que la clase de lectura
    static final int PORT_NUM_OUT = 30124;
    String ip = "";
    public String S = null;
    String cache = null;
    Socket calceta = null;
    public ClientThreadEntrada(){ }
    
    public ClientThreadEntrada(String ipAdress) 
    { 
        ip = ipAdress; 
    }
    
    public void run()
    {
      try 
      {
          InetAddress address = InetAddress.getLocalHost();
          Socket s1 = null;
          String line = null;
          BufferedReader is = null;
          //Se hacen las inicializaciones para definir la conexión
          try
          {
             if (ip.isEmpty())
             {
                 s1 = new Socket(address, PORT_NUM_OUT);
             } else {
                 s1 = new Socket(ip, PORT_NUM_OUT);
             }
             //La diferencia es que aquí se hace la lectura del flujo en sí, por lo que se usa 
             //El InputStreamReader, usando el flujo de salida del socket
             is = new BufferedReader(new InputStreamReader(s1.getInputStream()));
          } catch (IOException e){
              //igualmente en caso de error se cierran las conexiones
              e.printStackTrace();
              System.err.print("Error al inicializar proceso");
              try 
              {
                if (is != null)
                {
                  is.close();
                }
                if (s1 != null)
                {
                  s1.close();
                }
                System.out.println("Se ha cerrado la conexión");
              } catch (Exception ecp){
                  System.out.println(">! Ocurrió un error al intentar cerrar el Socket");
                  System.out.println(ecp.getMessage());
                  System.out.println("-------------------------------------------------");
              }
              return;
          }
          String response = null;
          //Posteriormente tenemos un ciclo semi infinito para mostrar
          //Que es lo que se recibe de servidor
          try
          {
              while(true)
              {
                  //Cuando llega algo al flujo de entrada
                  response = is.readLine();
                  //Este se manda a imprimir en consola
                  System.out.println(">:" + response);
              } 
                  
           }  catch (IOException e){
              e.printStackTrace();
              System.out.println("SocketInfo: Error de Lectura");  
          } finally 
          {
             try
             {
                 if (is != null){
                     is.close();
                 }
                 if(s1 != null){
                     s1.close();
                 }
             }
             catch(Exception ecp2){
                 System.out.println(">! Ocurrio un error al inbtentar cerrar el socket");
                 System.out.println(ecp2.getMessage());
                 System.out.println("-------------------------------------------------");
             }
          }
      } 
      catch(IOException e){
           System.out.println(">#! Error en una conexión"); 
      }
    }
    
    public String setEntrada(){
        return S;
    }
}*/

