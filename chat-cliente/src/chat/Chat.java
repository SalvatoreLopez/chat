
package chat;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.net.InetAddress;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;

public class Chat  extends JFrame{
 
    private static conexion conexion = new conexion();
    public JTextField baseDeDatos;
    public JTextField usuarioDB;
    public JPasswordField passwordDB;
    public JTextField ipDB;
    private JPanel panelP;
    private JButton botonConectar;
    public static void main(String[] args) throws Exception {
       login login = new login();
       login.setVisible(true);
       Chat ch = new Chat();
       ch.setVisible(true);
    }
    private  Chat(){
        super();
        propiedadesVentanaLogin();
        componentes();
    }
    private void propiedadesVentanaLogin(){
        this.setTitle("Configuracion db");                  
        this.setSize(230, 400);                              
        this.setResizable(false);                               
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);    
    }
    
      private void componentes(){
           panelP= new JPanel();
           panelP.setBounds(0, 0, 200, 100);
           baseDeDatos= new JTextField();
           usuarioDB= new JTextField();
           passwordDB= new JPasswordField();
           botonConectar = new JButton();
           ipDB= new JTextField();
           baseDeDatos.setBounds(20, 40, 170, 40); 
           usuarioDB.setBounds(20, 100, 170, 40); 
           passwordDB.setBounds(20, 160, 170, 40); 
           ipDB.setBounds(20, 220, 170, 40); 
           botonConectar.setBounds(20, 280, 170, 40);
           botonConectar.setText("Conectar");
           baseDeDatos.setText("chatbd");
           usuarioDB.setText("root");
           passwordDB.setText("");
           TextPrompt placeholderBaseDeDatos = new TextPrompt("Ingresa Base de datos", baseDeDatos);
           TextPrompt placeholderUsuario = new TextPrompt("Ingresa usuario de la DB", usuarioDB);
           TextPrompt placeholderPassword = new TextPrompt("Ingresa paswword de la DB", passwordDB);
           TextPrompt placeholderIp = new TextPrompt("Ingresa ip de la DB", ipDB);
           this.add(baseDeDatos);
           this.add(usuarioDB);
           this.add(passwordDB);
           this.add(ipDB);
           this.add(botonConectar);
           this.add(panelP);
           
           botonConectar.addActionListener((ActionEvent e) ->{
              if(!ipDB.getText().isEmpty() && !baseDeDatos.getText().isEmpty() && !usuarioDB.getText().isEmpty() && !ipDB.getText().isEmpty()){
                  try {
                      conexion.conexion(ipDB.getText(),usuarioDB.getText(), passwordDB.getText(), baseDeDatos.getText());
                  } catch (Exception ex) {
                      System.out.println("error "+ex);
                  }
              }else{
              JOptionPane.showMessageDialog(null, "Algun campo esta vacio");
              }
           });
           
           
      }
     
}
