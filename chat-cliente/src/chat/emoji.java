
package chat;

import java.util.ArrayList;
import java.util.StringTokenizer;
import javax.swing.ImageIcon;
import javax.swing.JTextPane;
import javax.swing.text.BadLocationException;
import javax.swing.text.Style;
import javax.swing.text.StyleConstants;
import javax.swing.text.StyleContext;
import javax.swing.text.StyledDocument;

/**
 *
 * @author salvatore
 */
public class emoji {
    
    public void emoji (JTextPane pane,String cadena){
        
        ArrayList lista = new ArrayList();
        StringTokenizer st = new StringTokenizer(cadena, ":", true);
        while (st.hasMoreTokens()) {
             lista.add(st.nextToken());
        }
        
        String[] initStyles = {"smile","joy","sad","sunglasses","sorprise","smirk","angry","kiss","sleeping","thinking","mask"};
        //Se crea un objeto para definir los estilos y se obtiene el textPane anterior
        StyledDocument doc = pane.getStyledDocument();
        //Se define un estilo como Default por cuestiones de configuración
        Style def = StyleContext.getDefaultStyleContext().getStyle(StyleContext.DEFAULT_STYLE);
        //Se crea un estilo definiendo que se agrega a los estilos del textPane
        //En este caso se llamará "smile"
        Style s = doc.addStyle("smile", def);
        //Se define una constante del estilo para centrar el icono
        StyleConstants.setAlignment(s, StyleConstants.ALIGN_CENTER);
        //Se crea un icono haciendo uso de una función, donde se envía la ruta de la imagen
        //      y tambien se le define el nombre
        ImageIcon icon = crearImageIcon("/images/Emoji Smiley-04.png", "smile");
        //Se asigna la constante al icono
        if (icon != null) {
            StyleConstants.setIcon(s, icon);
        }
        //repetimos el proceso para crear otros 2 emojis para este ejemplo
        s = doc.addStyle("joy", def);
        StyleConstants.setAlignment(s, StyleConstants.ALIGN_CENTER);
        icon = crearImageIcon("/images/Emoji Smiley-01.png", "joy");
        if (icon != null) {
            StyleConstants.setIcon(s, icon);
        }
        s = doc.addStyle("sad", def);
        StyleConstants.setAlignment(s, StyleConstants.ALIGN_CENTER);
        icon = crearImageIcon("/images/Emoji Smiley-17.png", "sad");
        if (icon != null) {
            StyleConstants.setIcon(s, icon);
        }
        s = doc.addStyle("sunglasses", def);
        StyleConstants.setAlignment(s, StyleConstants.ALIGN_CENTER);
        icon = crearImageIcon("/images/Emoji Smiley-03.jpg", "sunglasses");
        if (icon != null) {
            StyleConstants.setIcon(s, icon);
        }
        s = doc.addStyle("sorprise", def);
        StyleConstants.setAlignment(s, StyleConstants.ALIGN_CENTER);
        icon = crearImageIcon("/images/Emoji Smiley-05.png", "sorprise");
        if (icon != null) {
            StyleConstants.setIcon(s, icon);
        }
        s = doc.addStyle("smirk", def);
        StyleConstants.setAlignment(s, StyleConstants.ALIGN_CENTER);
        icon = crearImageIcon("/images/Emoji Smiley-06.png", "smirk");
        if (icon != null) {
            StyleConstants.setIcon(s, icon);
        }
        s = doc.addStyle("angry", def);
        StyleConstants.setAlignment(s, StyleConstants.ALIGN_CENTER);
        icon = crearImageIcon("/images/Emoji Smiley-07.png", "angry");
        if (icon != null) {
            StyleConstants.setIcon(s, icon);
        }
        s = doc.addStyle("kiss", def);
        StyleConstants.setAlignment(s, StyleConstants.ALIGN_CENTER);
        icon = crearImageIcon("/images/Emoji Smiley-08.png", "kiss");
        if (icon != null) {
            StyleConstants.setIcon(s, icon);
        }
        s = doc.addStyle("sleeping", def);
        StyleConstants.setAlignment(s, StyleConstants.ALIGN_CENTER);
        icon = crearImageIcon("/images/Emoji Smiley-09.png", "sleeping");
        if (icon != null) {
            StyleConstants.setIcon(s, icon);
        }
        s = doc.addStyle("thinking", def);
        StyleConstants.setAlignment(s, StyleConstants.ALIGN_CENTER);
        icon = crearImageIcon("/images/Emoji Smiley-10.png", "thinking");
        if (icon != null) {
            StyleConstants.setIcon(s, icon);
        }
         s = doc.addStyle("mask", def);
        StyleConstants.setAlignment(s, StyleConstants.ALIGN_CENTER);
        icon = crearImageIcon("/images/Emoji Smiley-11.png", "mask");
        if (icon != null) {
            StyleConstants.setIcon(s, icon);
        }
        //Se despedaza en este bloque la lista de String y define si se inserta o no
        //      el icono en cuestion en vez de :) o :D o :( 
        //Por lo que el FOR está definido para recorrer el largo de la lista
        //      que fue generada anteriormente en el ciclo while con los Tokens
        for (int i = 0; i < lista.size(); i++) {
            //En caso de error lo atrapamos en el TryCatch
            try{
                //Obtenemos en una variable String cada uno de los Strings resultantes para el análisis del texto
                String auxStr = (String) lista.get(i);
                
                //Averiguamos si el String en el que estamos es un simbolo de dos puntos y si hay otro objeto posterior
                //      para poder hacer la validación de que el siguiente caracter constituya uno de los emojis
                if (auxStr.equals(":") && (i+1) < lista.size() ) {
                    //Creamos otra variable String como auxiliar para validar el siguiente caracter
                    //      usando un substring
                    String auxStr2 = (String) lista.get(i+1);
                    //Verificamos que almenos tenga un caracter antes de revisar si el caracter concuerda con el faltante buscado
                    if(auxStr2.length()>0){
                        //En caso de que el caracter que siga sea un parentesis que cierra, será una carita con sonrisa
                        if (auxStr2.substring(0, 1).equals(")")) {
                            //Esta función en específico indica en que parte del texto hará la inserción
                            //  se indica con "doc.getLength()" para que seá hasta el final del texto
                            //  después el supuesto texto que se inserta, en este caso ' ":" + auxStr2.substring(0,1) '
                            //  por último el estilo que da formato al texto a insertar, el cuál lo reemplaza por un icono
                            doc.insertString(doc.getLength(), ":" + auxStr2.substring(0,1), doc.getStyle(initStyles[0]));
                        }
                        //En caso de que el caracter que siga sea una 'D', será una risa de alegría por así decirlo
                        if (auxStr2.substring(0, 1).toLowerCase().equals("d")) {
                            doc.insertString(doc.getLength(), ":" + auxStr2.substring(0,1), doc.getStyle(initStyles[1]));
                        }
                        //En caso de que el caracter que siga sea un parentesis que abre, será una carita triste
                        if (auxStr2.substring(0, 1).equals("(")) {
                            doc.insertString(doc.getLength(), ":" + auxStr2.substring(0,1), doc.getStyle(initStyles[2]));
                        }
                         //En caso de que el caracter que siga sea un parentesis que abre, será una carita triste
                        if (auxStr2.substring(0, 1).toLowerCase().equals("b")) {
                            doc.insertString(doc.getLength(), ":" + auxStr2.substring(0,1), doc.getStyle(initStyles[3]));
                        }
                        //En caso de que el caracter que siga sea un parentesis que abre, será una carita triste
                        if (auxStr2.substring(0, 1).toLowerCase().equals("o")) {
                            doc.insertString(doc.getLength(), ":" + auxStr2.substring(0,1), doc.getStyle(initStyles[4]));
                        }
                        //En caso de que el caracter que siga sea un parentesis que abre, será una carita triste
                        if (auxStr2.substring(0, 1).toLowerCase().equals("l")) {
                            doc.insertString(doc.getLength(), ":" + auxStr2.substring(0,1), doc.getStyle(initStyles[5]));
                        }
                        //En caso de que el caracter que siga sea un parentesis que abre, será una carita triste
                        if (auxStr2.substring(0, 1).equals("@")) {
                            doc.insertString(doc.getLength(), ":" + auxStr2.substring(0,1), doc.getStyle(initStyles[6]));
                        }
                        //En caso de que el caracter que siga sea un parentesis que abre, será una carita triste
                        if (auxStr2.substring(0, 1).equals("*")) {
                            doc.insertString(doc.getLength(), ":" + auxStr2.substring(0,1), doc.getStyle(initStyles[7]));
                        }
                        //En caso de que el caracter que siga sea un parentesis que abre, será una carita triste
                        if (auxStr2.substring(0, 1).toLowerCase().equals("z")) {
                            doc.insertString(doc.getLength(), ":" + auxStr2.substring(0,1), doc.getStyle(initStyles[8]));
                        }
                        //En caso de que el caracter que siga sea un parentesis que abre, será una carita triste
                        if (auxStr2.substring(0, 1).equals("?")) {
                            doc.insertString(doc.getLength(), ":" + auxStr2.substring(0,1), doc.getStyle(initStyles[9]));
                        }
                          //En caso de que el caracter que siga sea un parentesis que abre, será una carita triste
                        if (auxStr2.substring(0, 1).toLowerCase().equals("p")) {
                            doc.insertString(doc.getLength(), ":" + auxStr2.substring(0,1), doc.getStyle(initStyles[10]));
                        }
                    }
                    //Posteriormente se termina de insertar el texto restante y se recorre una posición porque ya se agregó lo que
                    //  estaba en la posición i+1
                    doc.insertString(doc.getLength(), auxStr2.substring(1)+"\n", null);
                    i++;
                }else{
                    //Solo se inserta el exto en caso de no haber algo que identificar
                    doc.insertString(doc.getLength(), auxStr+"\n", null);
                }
            }catch(Exception err){
            }
        }
    }
    
      /**
     * Regresa una ImageIcon, o null si la ruta del archivo no es valida.
     */
    protected static ImageIcon crearImageIcon(String url, String desc) {
        java.net.URL imgURL = emoji.class.getResource(url);
        if (imgURL != null) {
            return new ImageIcon(imgURL, desc);
        } else {
            System.err.println("No se encontró el archivo: " + url);
            return null;
        }
    }
}
