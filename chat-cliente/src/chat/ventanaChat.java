
package chat;

import java.awt.Color;
import java.awt.Container;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.net.Socket;
import javax.swing.BorderFactory;
import javax.swing.GrayFilter;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.ScrollPaneConstants;
import java.awt.BorderLayout;
import java.awt.Dimension;
import javax.swing.JTextPane;

public  class ventanaChat extends JFrame {
    hexadecimalArgb hrgb = new hexadecimalArgb();
    public JTextField mensaje;
    public JTextField nick;
    public JButton botonEnviar;
    public JTextPane cnv ;
    private JScrollPane panelP;
    public Container cContenedor;
    public String idNick = "";
    
    
    public ventanaChat( ) {  
        propiedadesVentana();   
        this.setUndecorated(false);
    }

     private void propiedadesVentana(){
         
        this.setTitle("Esta es la interfaz del chat");                        
        this.setLocationRelativeTo(null);  
        this.getContentPane().setBackground(new Color(hrgb.hexadecimalARGB(hrgb.blanco)));                              
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        componentes();
     }
     
     private void componentes(){
         login log = new login();
         idNick = log.idNick;
         cContenedor= getContentPane();
         cContenedor.setLayout(null);
         setResizable(false);
         mensaje = new JTextField();
         nick = new JTextField();
         botonEnviar = new JButton();
         mensaje.setBounds(30, 625, 290, 30); 
         TextPrompt placeholder = new TextPrompt("Escribe tu mensaje ", mensaje);
         mensaje.setForeground(new Color(hrgb.hexadecimalARGB(hrgb.grisClaro)));
         mensaje.setBorder(BorderFactory.createMatteBorder(0, 0, 0, 0, new Color(hrgb.hexadecimalARGB(hrgb.grisClaro))));
         mensaje.setBackground(new Color(hrgb.hexadecimalARGB(hrgb.fondos)));
         nick.setBounds(15, 10, 440, 35);
         nick.setText(log.idNick.split("/")[1]);
         nick.setForeground(new Color(hrgb.hexadecimalARGB("474747")));
         nick.setFont(new Font("Arial",Font.BOLD,14));
         nick.setBackground(new Color(hrgb.hexadecimalARGB(hrgb.fondos)));
         nick.setOpaque(true);
         botonEnviar.setBounds(340, 630, 110, 20);
         botonEnviar.setText("ENVIAR");
         botonEnviar.setBorderPainted(false);
         botonEnviar.setContentAreaFilled(false);
         botonEnviar.setForeground(new Color(hrgb.hexadecimalARGB(hrgb.grisClaro)));
         botonEnviar.setBackground(new Color(hrgb.hexadecimalARGB("474747")));
          final ImageIcon imageIcon = new ImageIcon("img.jpg");
         cnv = new JTextPane();
         cnv.setSize(700,750);
         cnv.setEditable(false);
         cnv.setBorder(BorderFactory.createMatteBorder(1, 1, 1, 1, new Color(hrgb.hexadecimalARGB(hrgb.grisOscuro))));
         panelP= new JScrollPane(cnv);
         //panelP.setViewportView();
         panelP.setBounds(15, 50, 440, 560);
         panelP.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
         panelP.setPreferredSize(new Dimension(800, 600));
         //En este el tamaño minimo preferido
         panelP.setMinimumSize(new Dimension(150, 80));
         cContenedor.add(panelP,BorderLayout.CENTER);
         cContenedor.add(mensaje);
         cContenedor.add(nick);
         cContenedor.add(botonEnviar);
         setSize( 480,700 );
         setLocationRelativeTo(null); 
     } 
}