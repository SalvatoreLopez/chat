package chat;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.InetAddress;
import java.net.Socket;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JTextArea;
import javax.swing.text.BadLocationException;
import javax.swing.text.Style;
import javax.swing.text.StyleContext;
import javax.swing.text.StyledDocument;

public class ClientThreadSalida extends Thread  { 
    //Se define un puerto de conexión para la lectura de datos
    static final int PORT_NUM_IN = 30123; 
    String ip = "";
    String cache = null;
    Socket calceta = null;
    ventanaChat vt;
    String line = null; 
    //se definen los flujpos de lectura 
    BufferedReader br = null; 
    PrintWriter os = null;
    String lineas = null;
    String cadena = null;
    emoji emo = new emoji();
    public ClientThreadSalida() { 
    }
    
    public ClientThreadSalida(String ipAdress, ventanaChat lvC)
    { 
       //al construir el hilo, se define la IP a conectar 
       ip = ipAdress;
       vt=lvC;
  
    }


    public void run(){
      
        try
        {   
            //En caso de que la IP no sea proporcionada, se define que sea por default el localhost como la IP a conectar
            InetAddress address = InetAddress.getLocalHost(); 
            Socket s1 = null; 

            //Se define la IP y se crea el socket en el puerto definido
            try
            {
                if (ip.isEmpty()) 
                { 
                    s1 = new Socket(address, PORT_NUM_IN);
                }                 
                else 
                { 
                    s1 = new Socket(ip, PORT_NUM_IN); 
                }
                //Se definen los flujos de etrada y salida
                br = new BufferedReader(new InputStreamReader(System.in)); 
                os = new PrintWriter(s1.getOutputStream());
            } catch (IOException e){
               //en caso de fallar, se cierran las conexiones y lecturas de flujos
               e.printStackTrace(); 
               System.err.print("Error al inicializar proceso");
               try
               {
                   if (os != null)
                   {
                      os.close(); 
                   }
                   if(br != null)
                   {
                       br.close();
                   }
                   if(s1 != null)
                   {
                       s1.close();
                   }
                   System.out.println("Se ha cerrado la conexión");
               } catch (Exception ecp){
                 System.out.println(">! Ocurrió un error al intentar cerrar el Socket");
                 System.out.println(ecp.getMessage());
                 
                 System.out.println("-------------------------------------------------");
               }
               return;
            }
            //Muestra la conexión local
            System.out.println("Dirección : " + address);
            String response = null;
            line = vt.nick.getText();
             chatDP cdp = new chatDP();
            //vt.cnv.append("has cambiado su nombre a -> "+ line+"\n");
            os.println("SYS_SET_NICK" + line);
            //Se limpia el flujo
            os.flush();
            vt.nick.addKeyListener(new KeyAdapter() {
                public void keyPressed(KeyEvent e) {
                   if(e.getKeyCode() == KeyEvent.VK_ENTER){
                       if(!vt.nick.getText().isEmpty())
                           
                            line = vt.nick.getText();
                       try {
                             cdp.actualizarNick(Integer.parseInt(vt.idNick.split("/")[0]), line);
                             StyledDocument doc = vt.cnv.getStyledDocument();
                             Style def = StyleContext.getDefaultStyleContext().getStyle(StyleContext.DEFAULT_STYLE);
                             doc.insertString(doc.getLength(),vt.nick.getText()+"   ha cambiado su nombre a: "+ line+"\n", null );
                            os.println("SYS_SET_NICK" + line);
                            //Se limpia el flujo
                            os.flush();
                       } catch (SQLException ex) {
                           System.out.println("error"+ex);
                       } catch (BadLocationException ex) {
                           Logger.getLogger(ClientThreadSalida.class.getName()).log(Level.SEVERE, null, ex);
                       }
                          
                   }
               }
            });
           
               vt.botonEnviar.addActionListener((ActionEvent e) -> {
                   if(!vt.mensaje.getText().isEmpty()){
                       lineas = vt.mensaje.getText();
                       cadena = line+"\n"+lineas;
                       emo.emoji(vt.cnv,cadena);
                       os.println(lineas);
                       os.flush();
                       vt.mensaje.setText("");
                   }
                       
               });
               vt.mensaje.addKeyListener(new KeyAdapter() {
                    public void keyPressed(KeyEvent e) {
                       if(e.getKeyCode() == KeyEvent.VK_ENTER){
                            if(!vt.mensaje.getText().isEmpty())
                               lineas = vt.mensaje.getText();
                               cadena = line+"\n"+lineas;
                               emo.emoji(vt.cnv, cadena);
                               os.println(lineas);
                               os.flush();
                               vt.mensaje.setText("");
                       }
                   }
        });
            
        } 
        catch(Exception e){
            System.out.println(e);
        }
    }  

 

    
}