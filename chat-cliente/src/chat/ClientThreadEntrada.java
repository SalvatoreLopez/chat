package chat;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.InetAddress;
import java.net.Socket;

public class ClientThreadEntrada extends Thread {
    //Se definen las variables al igual que la clase de lectura
    static final int PORT_NUM_OUT = 30124;
    String ip = "";
    String cache = null;
    Socket calceta = null;
    String cadena = null;
    ventanaChat vt;
    emoji emo = new emoji();
    
    public ClientThreadEntrada(){ }
    
    public ClientThreadEntrada(String ipAdress, ventanaChat lvC) 
    { 
        ip = ipAdress; 
        vt=lvC;
    }
    
    public void run()
    {
      
      try 
      {
          InetAddress address = InetAddress.getLocalHost();
          Socket s1 = null;
          String line = null;
          BufferedReader is = null;
          //Se hacen las inicializaciones para definir la conexión
          try
          {
             if (ip.isEmpty())
             {
                 s1 = new Socket(address, PORT_NUM_OUT);
             } else {
                 s1 = new Socket(ip, PORT_NUM_OUT);
             }
             //La diferencia es que aquí se hace la lectura del flujo en sí, por lo que se usa 
             //El InputStreamReader, usando el flujo de salida del socket
             is = new BufferedReader(new InputStreamReader(s1.getInputStream()));
          } catch (IOException e){
              //igualmente en caso de error se cierran las conexiones
              e.printStackTrace();
              System.err.print("Error al inicializar proceso");
              try 
              {
                if (is != null)
                {
                  is.close();
                }
                if (s1 != null)
                {
                  s1.close();
                }
                System.out.println("Se ha cerrado la conexión");
              } catch (Exception ecp){
                  System.out.println(">! Ocurrió un error al intentar cerrar el Socket");
                  System.out.println(ecp.getMessage());
                  System.out.println("-------------------------------------------------");
              }
              return;
          }
          String response = null;
          //Posteriormente tenemos un ciclo semi infinito para mostrar
          //Que es lo que se recibe de servidor
          try
          {   
              while(true)
              {
                  //Cuando llega algo al flujo de entrada
                  response = is.readLine();
                  //Este se manda a imprimir en consola
                  String cadena = "SYS_SET_NICK";
                  boolean resultado = response.contains(cadena);
                 if(resultado){
                      cadena = response.toString().split("SYS_SET_NICK")[1]+" ha iniciado sesion";
                      emo.emoji( vt.cnv, cadena);
                 }
                      
                 else{
                    emo.emoji( vt.cnv, response); 
                  System.out.println(">:" + response);
                 }
              } 
                  
           }  catch (IOException e){
              e.printStackTrace();
              System.out.println("SocketInfo: Error de Lectura");  
          } finally 
          {
             try
             {
                 if (is != null){
                     is.close();
                 }
                 if(s1 != null){
                     s1.close();
                 }
             }
             catch(Exception ecp2){
                 System.out.println(">! Ocurrio un error al intentar cerrar el socket");
                 System.out.println(ecp2.getMessage());
                 System.out.println("-------------------------------------------------");
             }
          }
      } 
      catch(IOException e){
           System.out.println(">#! Error en una conexión"); 
      }
    }
    
 
}

