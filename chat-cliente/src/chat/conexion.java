
package chat;
import java.sql.*;
import javax.swing.*;
public class conexion {

    public  static Connection conexion;
    
    public void conexion(String ip, String usuario, String password,String nombreDB) throws Exception{
        try {
            String url= "jdbc:mysql://"+ip+"/";
            Class.forName("com.mysql.jdbc.Driver");
            this.conexion = DriverManager.getConnection(url+nombreDB,usuario,password);
            JOptionPane.showMessageDialog(null, "Se conecto correctamente");
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null,"Error algo salio mal"+ e);
        }
    }
    
    public Connection getConnection(){
        return conexion;
    }
}
