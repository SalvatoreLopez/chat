
package chat;

import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;

public class mensajeUsuarioCreado extends JFrame{
    //mandamos a llamar la clase hexadecimalArgb
    hexadecimalArgb hrgb = new hexadecimalArgb();
    
    JButton botonAceptar;
    JLabel titulomensaje;
    public  mensajeUsuarioCreado() {
        super();
        propiedadesMensjaeExito();
        componetes();
        this.setUndecorated(true);
    }
    
    public void propiedadesMensjaeExito(){
               
        this.setSize(300, 200);                                 
        this.setLocationRelativeTo(null);                       
        this.setLayout(null);
        this.getContentPane().setBackground(new Color(hrgb.hexadecimalARGB(hrgb.grisOscuro)));
        this.setResizable(false);                               
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    }
    
    public void componetes(){
        botonAceptar = new JButton();
        titulomensaje = new JLabel("Usuario Creado con exíto");
        titulomensaje.setVisible(true);
        titulomensaje.setForeground(new Color (hrgb.hexadecimalARGB(hrgb.grisTitulo)));
        titulomensaje.setFont(new Font("Arial",Font.BOLD,20));
        titulomensaje.setBounds(30, 50, 250, 40);
        botonAceptar.setText("Aceptar");
        botonAceptar.setBounds(100, 130, 100, 40);
        botonAceptar.setBackground(new Color(hrgb.hexadecimalARGB(hrgb.azul)));
        botonAceptar.setForeground(new Color(hrgb.hexadecimalARGB(hrgb.blanco)));
        botonAceptar.setBorderPainted(false);
        this.add(botonAceptar);
        this.add(titulomensaje);
        
        botonAceptar.addActionListener((ActionEvent e) -> {
            super.dispose();
            login login = new login();
            login.setVisible(true);
        });
        
    }
}
