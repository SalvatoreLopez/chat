package chat;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import javax.swing.JOptionPane;


public class chatDP {
    private final conexion cn = new conexion();
    private Statement st;
    private ResultSet rs;
    ventanaRegistrar vr = new ventanaRegistrar();
    
    public void RegistroDP(String nombre, String nick, String contraseña) throws SQLException{
        String Query;
        if(!nombre.isEmpty() && !nick.isEmpty() && !contraseña.isEmpty()){
            Query = "SELECT * FROM usuarios where Nombre='" + nombre + "' and Contraseña='" + contraseña + "'";
            String resultadoBusqueda = "";
            if(cn.getConnection() != null){
                st = cn.getConnection().createStatement();
                rs = st.executeQuery(Query);
                while (rs.next()) {
                 resultadoBusqueda += rs.getString(1);
                }
            if(!"".equals(resultadoBusqueda)){
                JOptionPane.showMessageDialog(null, "Ya existe este usuario por favor intenta con otro");
            }
                else{
                      Query = "INSERT INTO usuarios (IdUsuario, Nombre, Contraseña, nick) VALUES (NULL,?,?,?)";
                      PreparedStatement ps;
                          ps = cn.getConnection().prepareStatement(Query);
                          ps.setString(1, nombre);
                          ps.setString(2, contraseña);
                          ps.setString(3, nick);
                          if(ps.executeUpdate() == 1){
                              System.out.println("correcto");
                              JOptionPane.showMessageDialog(null, "Se ha creado correctamente el usuario");
                          }
                          else{
                              System.out.println("Error");
                              JOptionPane.showMessageDialog(null, "No se pudo crear el usuario");
                          }
                }
            }
            else{JOptionPane.showMessageDialog(null, "Por favor conecta con la base de datos");}
          
        }
         else {
            System.out.println("Error !!!");
            JOptionPane.showMessageDialog(null, "Algun campo esta vacio");
             
        }
    }
    
    public boolean LoginDP(String nombre, String contraseña) throws SQLException {
        String Query = "SELECT * FROM usuarios where Nombre='" + nombre + "' and Contraseña='" + contraseña + "'";
        String resultadoBusqueda = "";
        st = cn.getConnection().createStatement();
        rs = st.executeQuery(Query);
        while (rs.next()) {
            resultadoBusqueda += rs.getString(1);
        }
        return !"".equals(resultadoBusqueda);
    }
    
    public String obtenerNick(String nombre, String contraseña) throws SQLException {
        String query = "SELECT IdUsuario, nick FROM usuarios where Nombre='" + nombre + "' and Contraseña='" + contraseña + "'";
        st = cn.getConnection().createStatement();
        rs = st.executeQuery(query);
        String cadena = "";
        while (rs.next()) {
              cadena+= rs.getString(1)+"/"+rs.getString(2);
        }
        return cadena;
    }
    public void actualizarNick(int id, String nick ) throws SQLException{
        String Query = "UPDATE usuarios SET nick=? WHERE IdUsuario='"+id+"'";
        PreparedStatement ps;
         if(cn.getConnection() != null){
                    ps = cn.getConnection().prepareStatement(Query);
                    ps.setString(1, nick);
                    ps.executeUpdate(); 
                     if(ps.executeUpdate() == 1){
                        
                        System.out.println("correcto");
                       // JOptionPane.showMessageDialog(null, "Se ha actualizado el nick correctamente  del usuario");
                        
                    }
                    else{
                        System.out.println("Error");
                        JOptionPane.showMessageDialog(null, "No se pudo actualizar el nick del  usuario");
                    }
                }
                else{JOptionPane.showMessageDialog(null, "Por favor conecta con la base de datos");}
    }
}
