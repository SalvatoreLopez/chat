
package chat;

import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPasswordField;
import javax.swing.JTextField;

public class ventanaRegistrar extends JFrame{
    //mandamos a llamar la clase hexadecimalArgb
    hexadecimalArgb hrgb = new hexadecimalArgb();
    
    JLabel tituloLogin1;
    JLabel tituloLogin2;
    JTextField usuario;
    JTextField nick;
    JPasswordField password;
    JButton botonEntrar;
    JButton botonRegistrar;
    JButton botonCerrarLogin;
    JButton botonRegresar;
    
    
     public  ventanaRegistrar() {
        super();
        propiedadesVentanaRegistrar();
        componetes();
        this.setUndecorated(true);
    }
     
     public void propiedadesVentanaRegistrar(){
        this.setTitle("Bienvenido al panel registrar");                  
        this.setSize(630, 450);                                 
        this.setLocationRelativeTo(null);                       
        this.setLayout(null);
        this.getContentPane().setBackground(new Color(hrgb.hexadecimalARGB(hrgb.grisOscuro)));
        this.setResizable(false);                               
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
     
     }
     
     //componentes
    private void componetes(){
        tituloLogin1 = new JLabel("Aquí puedes");
        tituloLogin2 = new JLabel("Registrarte!");
        usuario = new JTextField();
        nick = new JTextField();
        password = new JPasswordField();
        botonRegistrar = new JButton();
        botonCerrarLogin = new JButton();
        botonRegresar = new JButton();
        tituloLogin1.setVisible(true);
        tituloLogin1.setForeground(new Color (hrgb.hexadecimalARGB(hrgb.grisTitulo)));
        tituloLogin1.setFont(new Font("Arial",Font.BOLD,25));
        tituloLogin1.setBounds(150, 100, 200, 40);
        tituloLogin2.setVisible(true);
        tituloLogin2.setForeground(new Color (hrgb.hexadecimalARGB(hrgb.azul)));
        tituloLogin2.setFont(new Font("Arial",Font.BOLD,25));
        tituloLogin2.setBounds(310, 100, 150, 40);
        tituloLogin1.setForeground(new Color(hrgb.hexadecimalARGB(hrgb.grisClaro)));
        TextPrompt placeholderUsuario = new TextPrompt("Usuario", usuario);
        TextPrompt placeholderPassword = new TextPrompt("Password", password);
        TextPrompt placeholderNick= new TextPrompt("Nick", nick);
        usuario.setBounds(100, 170, 400, 40);
        usuario.setFont(new Font("Arial",Font.BOLD,14));
        usuario.setBackground(new Color(hrgb.hexadecimalARGB(hrgb.grisTitulo)));
        usuario.setBorder(BorderFactory.createMatteBorder(0, 0, 2, 0, new Color(hrgb.hexadecimalARGB(hrgb.grisTitulo))));
        usuario.setBackground(new Color(hrgb.hexadecimalARGB(hrgb.grisOscuro)));
        nick.setBounds(100, 220, 400, 40);
        nick.setFont(new Font("Arial",Font.BOLD,14));
        nick.setBackground(new Color(hrgb.hexadecimalARGB(hrgb.grisTitulo)));
        nick.setBorder(BorderFactory.createMatteBorder(0, 0, 2, 0, new Color(hrgb.hexadecimalARGB(hrgb.grisTitulo))));
        nick.setBackground(new Color(hrgb.hexadecimalARGB(hrgb.grisOscuro)));
        password.setBounds(100, 270, 400, 40);
        password.setBorder(BorderFactory.createMatteBorder(0, 0, 2, 0, new Color(hrgb.hexadecimalARGB(hrgb.grisTitulo))));
        password.setBackground(new Color(hrgb.hexadecimalARGB(hrgb.grisOscuro)));
        password.setFont(new Font("Arial",Font.BOLD,14));
        botonRegistrar.setText("Crear Cuenta");
        botonRegistrar.setBounds(170, 350, 120, 40);
        botonRegistrar.setBackground(new Color(hrgb.hexadecimalARGB(hrgb.azul)));
        botonRegistrar.setForeground(new Color(hrgb.hexadecimalARGB(hrgb.blanco)));
        botonRegistrar.setBorderPainted(false);
        
        botonRegresar.setText("Regresar");
        botonRegresar.setBounds(300, 350, 120, 40);
        botonRegresar.setBackground(new Color(hrgb.hexadecimalARGB(hrgb.azul)));
        botonRegresar.setForeground(new Color(hrgb.hexadecimalARGB(hrgb.blanco)));
        botonRegresar.setBorderPainted(false);
        this.add(tituloLogin1);
        this.add(tituloLogin2);
        this.add(usuario);
        this.add(password);
        this.add(botonRegistrar);
        this.add(nick);
        this.add(botonRegresar);
        
        botonRegistrar.addActionListener((ActionEvent e) -> {
            chatDP registro =new chatDP();
            try {
                if(!usuario.getText().isEmpty() && !nick.getText().isEmpty() && !password.getText().isEmpty()){
                    registro.RegistroDP(usuario.getText(),nick.getText(), password.getText());
                }
                else{
                    JOptionPane.showMessageDialog(null,"Campos vacíos");
                }
            } catch (SQLException ex) {
                Logger.getLogger(ventanaRegistrar.class.getName()).log(Level.SEVERE, null, ex);
            }
        });
        
         botonRegresar.addActionListener((ActionEvent e) -> {
            super.dispose();
            login vl = new login();
            vl.setVisible(true);
        });
    }
}
