
package Chat;
import java.awt.Color;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

public class interfazServidor extends JFrame {
    public JTextField mostrarIp;
    private JPanel panelP;
    public JLabel titulo;
    public void interfazServidor(String ip){
        
        propiedadesVentana(ip);
        this.setUndecorated(false);
    }
    public void propiedadesVentana(String ip){                 
        this.setSize(200, 130);
        this.setLayout(null);
        this.setResizable(false);                               
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        componentes(ip);
     
    }
      private void componentes(String ip){
           panelP= new JPanel();
           panelP.setBounds(0, 0, 200, 100);
           mostrarIp= new JTextField();
           mostrarIp.setBounds(10, 40, 170, 40); 
           mostrarIp.setText(ip);
           titulo = new JLabel();
           titulo.setBounds(10, 5, 170, 40); 
           titulo.setText("La ip del servidor es: ");
           this.add(mostrarIp);
           this.add(titulo);
           this.add(panelP);
      }
 
}
