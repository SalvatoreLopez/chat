package Chat;
import static Chat.Servidor.conexiones;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import javax.swing.JOptionPane;

public class Servidor {

    static final int PORT_NUM_IN = 30124;
    static final int PORT_NUM_OUT = 30123;

    public static ArrayList conexiones;

    public static void main(String args[]) {
       

        Socket sOut = null;
        Socket sIn = null;
        ServerSocket servidorIn = null;
        ServerSocket servidorOut = null;
        conexiones = new ArrayList();
        
        System.out.println("> Se ha iniciado el servidor, en espera de clientes....");
        
        try {
            InetAddress address = InetAddress.getLocalHost();
            servidorIn = new ServerSocket(PORT_NUM_IN); // can also use , when defined
            servidorOut = new ServerSocket(PORT_NUM_OUT); // can also use , when defined
            System.out.println("> Dirección : " + address);
            
            interfazServidor is = new interfazServidor();
            is.interfazServidor(address.toString().split("/")[1]);
            is.setVisible(true);
            while (true) {
                try {
                    sOut = servidorOut.accept();
                    sIn = servidorIn.accept();
                    ServerThread sto = new ServerThread(sOut, sIn);
                    sto.start();
                    conexiones.add(sto);
                    System.out.println("> Se ha establecido una conexión");
                } catch (Exception e) {
                    e.printStackTrace();
                    System.out.println("> Hubo un error en la conexión");

                }
            }

        } catch (IOException e) {
            System.out.println(">>! Error en la inicialización del servidor...");
            e.printStackTrace();
        }

    }

}

class ServerThread extends Thread {

    String cache = null;
    BufferedReader inputCache = null;
    PrintWriter outputCache = null;
    Socket SocketEntrada = null;
    Socket SocketSalida = null;

    public ServerThread(Socket entrada, Socket salida) {
        this.SocketEntrada = entrada;
        this.SocketSalida = salida;
    }

    public void run() {
        try {
            inputCache = new BufferedReader(new InputStreamReader(SocketEntrada.getInputStream()));
            outputCache = new PrintWriter(SocketSalida.getOutputStream());

            try {
                cache = inputCache.readLine();
                while (cache.compareTo("EXIT") != 0) {
                    for (int i = 0; i < conexiones.size(); i++) {
                        ServerThread st = (ServerThread) conexiones.get(i);
                        if (!this.getName().equals(st.getName())) {
                            st.outputCache.println("> " + this.getName() + " : " + cache);
                            st.outputCache.flush();
                        } else {
                            if(cache.length() > 2)
                            if (cache.substring(0, 2).contains("NN")) {
                                st.setName(cache.substring(2));
                                st.outputCache.println("Nombre cambiado a \"" + cache.substring(2) + "\"");
                                st.outputCache.flush();
                            }
                            
                            if(cache.length() > 12)
                            if (cache.substring(0,"SYS_SET_NICK".length()).contains("SYS_SET_NICK")) {
                            st.setName(cache.substring("SYS_SET_NICK".length()));
                            }

                        }
                    }
                    //outputCache.println(cache);
                    outputCache.flush();
                    System.out.println("> " + this.getName() + " : " + cache);
                    cache = inputCache.readLine();
                }
            } catch (IOException e) {
                cache = this.getName();
                System.out.println(">>! IO Error/ Cliente " + cache + " ha cerrado la conexión inesperadamente");
            } catch (NullPointerException e) {
                cache = this.getName();
            } finally {
                try {
                    System.out.println("# Cerrando conexión...");
                    if (inputCache != null) {
                        inputCache.close();
                    }
                    if (outputCache != null) {
                        outputCache.close();
                    }
                    if (SocketEntrada != null) {
                        SocketEntrada.close();
                    }
                    if (SocketSalida != null) {
                        SocketSalida.close();
                    }

                } catch (IOException ie) {
                    System.out.println("# SocketInfo - Error al intentar cerrar el socket");
                    System.out.println(ie.getMessage());
                    System.out.println("------------------------------------------------");
                }
            }

        } catch (IOException e) {
            System.out.println(">#! Error en una conexión");
        }
    }
}